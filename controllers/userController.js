const User = require('../models/User');
const jwt = require('jsonwebtoken');
const { registerValidation, loginValidation } = require('../validation');

// Registre a new user
exports.register = async (req, res) => {

    // VALIDATE THE DATA BEFORE WE MAKE A USER
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // Cheking if the user already existe in database
    const emailExist = await User.findOne({ email: req.body.email });
    if (emailExist) return res.status(400).send('Email already exist');

    // Create a new user
    const user = new User({
        id: req.body.id,
        name: req.body.name,
        email: req.body.email,
        role: req.body.role,
    });
    try {
        const savedUser = await user.save();
        res.send(savedUser);
    } catch (err) {
        res.status(400).send(err);
    }
};

// Login
exports.login = async (req, res) => {
    // VALIDATE THE DATA BEFORE WE LOG A USER
    const { error } = loginValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    // Cheking if the email exist
    const user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(400).send('Email not found');

    // Create and assigna Token
    const token = jwt.sign({ id: user.id }, process.env.TOKEN_SECRET);
    res.header('auth-token', token).send(token);
};


// Get user by id
exports.getUserById = async (req, res) => {
    try {
        const userId = req.params.id;
        const user = await User.findOne({ id: userId });
        if (!user) {
            return res.status(404).send({ error: "User not found" });
        }
        res.send({ user });
    } catch (error) {
        res.status(400).send(error);
    }
};

// Get user by name
exports.getUserByName = async (req, res) => {
    try {
        const { name } = req.body;
        const user = await User.findOne({ name });
        if (!user) {
            return res.status(404).send({ error: "User not found" });
        }
        res.send({ user });
    } catch (error) {
        res.status(400).send(error);
    }
};
