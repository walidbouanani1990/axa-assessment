const express = require('express');
const app = express();

// Database connection
require("./db/db");

// Import routes
const authRoute = require('./routes/auth'); 
const userRouter = require('./routes/user');

// Middleware
app.use(express.json());

// Route Middleware
app.use('/api/auth', authRoute);
app.use('/api', userRouter);

app.listen(3000, () => console.log('The server is running'));