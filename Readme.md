# AXA Clients Management Api

## Description
API REST that allows to get clients axa informations.

Database : MongoDB Atlas.

## Features
| Endpoint   |      Type      |      Body      |  Description |
|------------|:--------------:|:--------------:|-------------:|
| `api/auth/register` |  POST | `{name: "user name", email: "user email"}` | Create a new user |
| `api/auth/login` |  POST | `{email: "user email"}` | Login and affect a token |
| `api/users/:id` |  GET | none | Get user by id |
| `api/users` |    GET   | `{name: "user name"}` |   Get user by name |

## Packages used
-   **Express.js**: A node.js framework that makes it easy to build web applications.
-   **mongodb**: Official MongoDB driver for Node.js.
-   **mongoose**: An object modeling tool designed to work in an asynchronous environment. We shall use mongoose to define database schemas and interact with the database.
-   **nodemon**: Nodemon will re-run the express server every time we make changes to our code.
-   **dotenv**: Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env. Storing configuration in the environment separate from code is based on The Twelve-Factor App methodology..
-   **jwt**: JSON Web Tokens are an open, industry standard RFC 7519 method for representing claims securely between two parties.
-   **@hapi/joi**: joi lets you describe your data using a simple, intuitive, and readable language. Like the rest of the hapi ecosystem it fits in, joi allows you to describe your data for both input and output validation, as part of a hapi HTTP server or standalone..

## Versions
-   Node.js **12.16.3**
-   Npm **6.14.4**
-   MongoDB **3.6+**

## How to install
### Using Git (recommended)
1.  Clone the project from github. Change "myproject" to your project name.
```bash
https://gitlab.com/walidbouanani1990/axa-assessment.git
```

### Install npm dependencies after installing (Git or manual download)
```bash
cd <myproject name>
npm install
```
### Setting up environments (development or production)
### Running in development mode (lifting API server)
```bash
npm start
```
You will know server is running by checking the output of the command `npm start`
```bash
The server is running
Connected to DB !
```

NB : I mistakenly pulled my branch from master at the begining of the project instead of develop
