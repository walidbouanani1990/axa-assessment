const express = require("express");
const User = require("../models/User");
const userController = require("../controllers/userController");

const router = express.Router();

router.get("/users/:id", userController.getUserById);

router.get("/users", userController.getUserByName);

module.exports = router;
