const router = require('express').Router();
const User = require('../models/User');

// Controllers
const userController = require("../controllers/userController");

// Register
router.post('/register', userController.register);

// Login
router.post('/login', userController.login);

module.exports = router;
